
function createNewUser() {
  const newUser = new Object();

  const firstName = prompt("Введіть Ваше ім'я");
  while (firstName === null || firstName === "" || !isNaN(firstName)) {
    firstName = prompt("Введіть Ваше ім'я", firstName);
  }

  const lastName = prompt("Введіть Ваше прізвище");
  while (lastName === null || lastName === "" || !isNaN(lastName)) {
    lastName = prompt("Введіть Ваше прізвище", lastName);
  }
         
  Object.defineProperties(newUser, {
    firstName: {
      value: firstName,
      configurable: true,
      writable: false,
    },
    lastName: {
      value: lastName,
      configurable: true,
      writable: false,
    },
  });

  newUser.getLogin = function () {
    return `${(newUser.firstName.charAt(0) + newUser.lastName).toLowerCase()}`;
    };

    return newUser;
    
}
let user = createNewUser();
console.log(user.getLogin());

